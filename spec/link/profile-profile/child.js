const Overwrite = require('@tangle/overwrite')

const overwrite = (schema, opts = {}) => Overwrite({ valueSchema: schema, ...opts })

module.exports = {
  type: 'link/profile-profile/child',
  staticProps: {
    child: { $ref: '#/definitions/messageId', required: true },
    parent: { $ref: '#/definitions/messageId', required: true }
  },
  props: {
    relationshipType: overwrite({
      enum: ['birth', 'whangai', 'adopted', 'unknown']
    }),
    legallyAdopted: overwrite({ type: 'boolean' })
  }
}
