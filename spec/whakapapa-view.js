const Overwrite = require('@tangle/overwrite')
const OverwriteFields = require('@tangle/overwrite-fields')
const SimpleSet = require('@tangle/simple-set')

const imageSchema = require('ssb-schema-definitions/definition/image')
const msgIdPattern = require('ssb-schema-definitions/definition/primitives').messageId.pattern

const overwrite = (schema, opts = {}) => Overwrite({ valueSchema: schema, ...opts })

const string = overwrite({ type: 'string' })
const image = overwrite(imageSchema)
const set = SimpleSet()

module.exports = {
  type: 'whakapapa/view',
  tangle: 'view',
  props: {
    name: overwrite({ type: 'string', minLength: 1 }),
    description: string,
    image,
    focus: overwrite({ type: 'string', pattern: msgIdPattern }),
    recordCount: overwrite({ type: 'number' }),

    /* rules */
    mode: overwrite({ enum: ['ancestors', 'descendants'] }),
    permission: overwrite({ enum: ['view', 'edit', 'submit'] }),
    ignoredProfiles: set, // TODO lock down to msgIds
    importantRelationships: OverwriteFields({
      keyPattern: msgIdPattern,
      valueSchema: {
        type: 'array',
        items: {
          type: 'string',
          pattern: msgIdPattern
        }
      }
    })
  },
  hooks: {
    isRoot: [
      content => {
        if (typeof content.name?.set === 'string') return true
        return new Error('whakapapaView.name is required')
      }
    ]
  }
}
