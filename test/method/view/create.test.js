const test = require('tape')
const { isMsg } = require('ssb-ref')
const Server = require('../../test-bot')

test('view/create (success)', t => {
  const server = Server()
  t.plan(3)

  // old style
  const details = () => ({
    name: { set: 'Irving Family' },
    description: { set: 'just notes for me' },
    focus: { set: '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256' },
    mode: { set: 'descendants' },
    permission: { set: 'view' },
    ignoredProfiles: { add: ['%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'] },
    image: {
      set: {
        blob: '&07xxrn0wWbtRx/gp4IF7THPeWZUT1LWt93IVHTbitFE=.sha256',
        mimeType: 'image/png'
      }
    },
    recordCount: { set: 100 },
    importantRelationships: {
      '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256': [
        '%AAAkDwndSuf93EMK74Am8lljjIn2UvU00kQt/3qxehc=.sha256'
      ]
    },
    recps: [
      server.id, // me
      '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519'
    ],
    authors: {
      add: [server.id]
    }
  })

  server.whakapapa.view.create(details(), (err, id) => {
    t.equal(err, null, 'no error!')
    t.true(isMsg(id), 'calls back with a messageId for view')

    const expected = Object.assign(
      {
        type: 'whakapapa/view',
        tangles: {
          view: { root: null, previous: null }
        }
      },
      {
        ...details(),
        ignoredProfiles: {
          '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256': 1
        },
        authors: {
          [server.id]: {
            0: 1
          }
        }
      }
    )

    server.get({ id, private: true }, (_, value) => {
      t.deepEqual(value.content, expected, 'publishes exactly the right message!')
      server.close()
    })
  })
})

test('view/create (failure)', t => {
  const server = Server()
  t.plan(2)

  // old style
  const details = () => ({
    name: { set: 'Irving Family' },
    focus: { set: '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256' },
    mode: { set: 'whatever' }, // << invalid
    authors: { add: [server.id] },
    recps: [
      server.id, // me
      '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519'
    ],
    recordCount: { set: 100 }
  })

  server.whakapapa.view.create(details(), (err) => {
    t.match(err.message, /data.mode .*/, 'incorrect mode input, fails with helpful message')
  })

  // old style
  const details2 = () => ({
    name: { set: 'Irving Family' },
    focus: { set: '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256' },
    mode: { set: 'descendants' },
    permission: { set: 'read' }, // << invalid
    authors: { add: [server.id] },
    recps: [
      server.id, // me
      '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519'
    ],
    recordCount: { set: 100 }
  })

  server.whakapapa.view.create(details2(), (err) => {
    t.match(err.message, /data.permission .*/, 'incorrect permission input, fails with helpful message')

    server.close()
  })
})
