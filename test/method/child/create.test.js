const test = require('tape')
const { isMsg } = require('ssb-ref')
const Server = require('../../test-bot')

test('child/create (success)', t => {
  const server = Server()
  t.plan(3)

  const parent = '%aqH4m8lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const child = '%bazdatAeXPu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256'

  const opts = {
    recps: [
      server.id, // me
      '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519'
    ]
  }

  server.whakapapa.child.create({ parent, child }, opts, (err, id) => {
    t.error(err, 'no error')
    t.true(isMsg(id), 'calls back with a messageId for relationship')

    const expected = {
      type: 'link/profile-profile/child',

      parent,
      child,

      recps: opts.recps,
      tangles: {
        link: { root: null, previous: null }
      }
    }

    server.get({ id, private: true }, (_, value) => {
      t.deepEqual(value.content, expected, 'publishes exactly the right message!')
      server.close()
    })
  })
})

test('child/create (failure)', t => {
  const server = Server()
  t.plan(1)

  const parent = 'dave'
  const child = '%bazdatAeXPu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256'

  server.whakapapa.child.create({ parent, child }, null, (err) => {
    t.equal(err.message, 'data.parent referenced schema does not match', 'fails with helpful message')

    server.close()
  })
})
