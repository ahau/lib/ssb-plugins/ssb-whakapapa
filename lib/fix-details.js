module.exports = function fixDetails (details) {
  // legacy API!
  for (const key in details) {
    if (details[key] === null) continue
    if (typeof details[key] !== 'object') continue

    if ('set' in details[key]) details[key] = details[key].set
  }

  return details
}
