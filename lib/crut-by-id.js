module.exports = function CrutById (server, crutByType) {
  return function crutById (id, cb) {
    server.get({ id, private: true }, (err, value) => {
      if (err) return cb(err)

      const crut = crutByType(value.content.type)
      if (!crut) return cb(new Error('invalid type ' + value.content.type))

      cb(null, crut)
    })
  }
}
