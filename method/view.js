const Crut = require('ssb-crut-authors')
const Force = require('ssb-crut/lib/force')
const { promisify } = require('util')

const spec = require('../spec/whakapapa-view')
const fixDetails = require('../lib/fix-details')

module.exports = (ssb) => {
  const crut = new Crut(ssb, spec)
  const { forceUpdate, forceTombstone } = Force(crut)

  function get (viewId, cb) {
    if (cb === undefined) return promisify(get)(viewId)

    crut.read(viewId, (err, view) => {
      if (err) return cb(err)

      view.viewId = viewId // legacy support
      cb(null, view)
    })
  }

  return {
    ...crut,
    create (details, cb) {
      return crut.create(fixDetails(details), cb)
    },
    get,
    read: get,
    update (viewId, details, cb) {
      return forceUpdate(viewId, fixDetails(details), cb)
    },
    tombstone (viewId, details, cb) {
      return forceTombstone(viewId, fixDetails(details), cb)
    },
    list (opts, cb) {
      return crut.list(opts, cb)
    }
  }
}
