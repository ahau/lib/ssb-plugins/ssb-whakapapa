const Method = require('./method')

const crut = {
  create: 'async',
  get: 'async',
  update: 'async',
  tombstone: 'async',
  list: 'async'
}

module.exports = {
  name: 'whakapapa',
  version: require('./package.json').version,
  manifest: {
    get: 'async',
    view: {
      ...crut,
      read: 'async'
    },
    child: crut,
    partner: crut,

    /* deprecated */
    link: {
      ...crut,
      getLinksOfType: 'async'
    }
  },
  init: Method
}
